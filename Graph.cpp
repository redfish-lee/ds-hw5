/* 
 * DS-HW5 
 * Graph Minimal Spanning Tree
 * Redfish.tbc@gmail.com
 */ 
#include <list>
#include <algorithm>
#include "Graph.h"

Graph::Graph(int& NVertex){
    NumVertex = NVertex;
	
    for(int i = 0; i < NVertex; i++){
        list<AdjNode> NodeList;
        AdjList.push_back(NodeList);
    }
}

void Graph::AddNode(int& first, int& second, int& weight){
    AdjNode NewNode;
    /* add to term[first] */
    NewNode.index = second;
    NewNode.weight = weight;
    AdjList[first].push_back(NewNode);

    /* add to unsorted vector */
    Edge NewEdge;
    NewEdge.FirstNode = first;
    NewEdge.SecondNode = second;
    NewEdge.weight = weight;
    EdgeVec.push_back(NewEdge); 
	
    /* add to term[second] */
    NewNode.index = first;
    NewNode.weight = weight;
    AdjList[second].push_back(NewNode);
}

bool cycleCheck(Edge& newEdge, vector<Edge>& Answer){
// An undirected graph has a cycle iff DFS finds an edge 
// that points to an already-visited vertex (a back edge)
    
    return 0;
}

void Graph::MST(vector<Edge>& Answer){
/* Kruskal Algorithm */

    // Sort by non-decreasing order
    sort(EdgeVec.begin(), EdgeVec.end(), cmpWeight());

//------------------------- Union By Rank ----------------------------//
    
    int e = 0;  // An index variable, used for result[]
    int i = 0;  // An index variable, used for sorted edges
 
    // Allocate memory for creating V ssubsets
    struct subset *subsets =
        (struct subset*) malloc( NumVertex * sizeof(struct subset) );
 
    // Create V subsets with single elements
    for (int v = 0; v < NumVertex ; ++v)
    {
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }
 
    // Number of edges to be taken is equal to V-1
    while (e < NumVertex - 1)
    {
        // Step 2: Pick the smallest edge. And increment the index
        // for next iteration
        struct Edge next_edge = EdgeVec[i++];
 
        int x = find(subsets, next_edge.FirstNode);
        int y = find(subsets, next_edge.SecondNode);
 
        // If including this edge does't cause cycle, include it
        // in result and increment the index of result for next edge
        if (x != y)
        {
            Answer.push_back(next_edge);
            e++;
            Union(subsets, x, y);
        }
        // Else discard the next_edge
    }
 

//------------------------ my part----------------------------//
/* 
    Edge newEdge;
    for(int i = 0; i < EdgeVec.size(); i++){
   
        // add edge to ans until ans.size = NumVertex-1
        if(Answer.size() < NumVertex){
            if (cycleCheck(EdgeVec[i], Answer)){
                newEdge.FirstNode = i;

                newEdge.SecondNode = AdjList[i].end()->index;
                newEdge.weight = AdjList[i].end()->weight;
                Answer.push_back(newEdge);
            }
            else break;
        }
        else break; 
    }
*/
}

// A utility function to find set of an element i
int find(struct subset subsets[], int i)
{
    // find root and make root as parent of i (path compression)
    if (subsets[i].parent != i)
        subsets[i].parent = find(subsets, subsets[i].parent);
 
    return subsets[i].parent;
}
 
// A function that does union of two sets of x and y
void Union(struct subset subsets[], int x, int y)
{
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);
 
    // Attach smaller rank tree under root of high rank tree
    // Union by Rank
    if (subsets[xroot].rank < subsets[yroot].rank)
        subsets[xroot].parent = yroot;
    else if (subsets[xroot].rank > subsets[yroot].rank)
        subsets[yroot].parent = xroot;
 
    // If ranks are same, 
    // then make one as root and increment its rank by one
    else {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}

