/* 
 * DS-HW5 
 * Graph Minimal Spanning Tree
 * Redfish.tbc@gmail.com
 */ 
#ifndef _Graph_H_
#define _Graph_H_

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <list>

using namespace std;

struct Edge{
    int FirstNode, SecondNode, weight;
};

struct AdjNode{
    int index, weight;
};

struct subset{
// A structure to represent a subset for union-find
    int parent;
    int rank;
};

struct cmpWeight{
    inline bool operator()(const Edge& a, const Edge& b){
        return (a.weight < b.weight);
    }
};

int  find(struct subset subsets[], int i);
void Union(struct subset subsets[], int x, int y);

class Graph{
    private:
        int NumVertex;
        vector< list<AdjNode> > AdjList;
        vector<Edge> EdgeVec;
    public:
        Graph(int& NumVertex); /* Contructor */
        void AddNode(int& first, int& second, int& weight);
        void MST(vector<Edge>& Answer);
        bool cycleCheck(Edge& newEdge, vector<Edge>& Answer);
};

#endif

